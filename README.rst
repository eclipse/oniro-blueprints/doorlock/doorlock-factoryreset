.. _doorlock_factoryreset:

Doorlock Factory Reset
######################

Overview
********

Doorlock Factory Reset clears the non-volatile storage
on the board to perform a factory reset - e.g. to remove
a forgotten PIN.

.. _doorlock_factoryreset-requirements:

Building and Running
********************

Build and flash Doorlock as follows, changing ``arduino_nano_33_ble`` for your board:

.. zephyr-app-commands::
   :zephyr-app: doorlock-factoryreset
   :board: arduino_nano_33_ble
   :goals: build flash
   :compact:

After flashing, the storage on the board is wiped, and
all PINs etc. are lost. You have to reflash the doorlock
app afterwards.
