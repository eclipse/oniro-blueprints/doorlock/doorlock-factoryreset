/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#include <zephyr.h>
#include <sys/printk.h>
#include <string.h>
#include "debugconsole.h"
#include "storage.h"

void main(void)
{
	debugconsole_init();
	storage_init();

	nvs_clear(&storage_fs);
}
