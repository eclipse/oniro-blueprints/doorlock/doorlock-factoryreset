/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#include "storage.h"

struct nvs_fs storage_fs;

#define STORAGE_NODE DT_NODE_BY_FIXED_PARTITION_LABEL(storage)
#define FLASH_NODE DT_MTD_FROM_FIXED_PARTITION(STORAGE_NODE)

int storage_init() {
	struct flash_pages_info info;
	const struct device *flash_dev;

	flash_dev = DEVICE_DT_GET(FLASH_NODE);
	if(!device_is_ready(flash_dev)) {
		printk("Flash device %s is not ready\n", flash_dev->name);
		return 1;
	}

	int rc;
	storage_fs.offset = FLASH_AREA_OFFSET(storage);
	rc = flash_get_page_info_by_offs(flash_dev, storage_fs.offset, &info);
	if(rc) {
		printk("Unable to get page info from %s\n", flash_dev->name);
		return 1;
	}

	storage_fs.sector_size = info.size;
	storage_fs.sector_count = 3U;

	rc = nvs_init(&storage_fs, flash_dev->name);
	if(rc) {
		printk("Unable to initialize nvs on %s\n", flash_dev->name);
		return 1;
	}
	return 0;
}
